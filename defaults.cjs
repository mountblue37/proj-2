function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    let ans = {...obj};
    if(typeof obj === "object" && obj !== null && !Array.isArray(obj))
        for(let key in defaultProps) ans[key] = defaultProps[key];
    return ans;
}

module.exports = defaults;