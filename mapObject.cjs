function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    let mappedObj = {};
    if(typeof obj === "object" && obj !== null && !Array.isArray(obj))
        for(let key in obj) mappedObj[key] = cb(obj[key],key);
    return mappedObj;
}

module.exports = mapObject;