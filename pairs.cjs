function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    let list = [];
    if(typeof obj === "object" && obj !== null && !Array.isArray(obj))
        for(let key in obj) list.push([key,obj[key]]);
    return list;
}

module.exports = pairs;