function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    let values = [];
    if(typeof obj === "object" && obj !== null && !Array.isArray(obj))
        for(let key in obj) values.push(obj[key]);
    return values;
}

module.exports = values;